# Task Manager App

## Setup
Must have a running Mongodb Connection!
Run the following command in the background for mongodb connection
```
C:/Users/<user>/mongodb/bin/mongod.exe --dbpath=C:/Users/<user>/mongodb-data
```

Run `npm install` on the root directory

## Run the Application
Run `node src/index.js` on the root directory

## Access the Web Application on Postman
{{url}} = localhost:3000

## USAGE!
User must need to create a user and login first before accessing endpoints. 
They can also just access their data, and not the data of other users.

## Enviroment!
url: localhost:3000

authtoken: Coded to tests for Create User and Login User Endpoints
```
if(pm.response.code === 201){
    pm.environment.set('authToken', pm.response.json().token)
}
```


### [POST] Create User
{{url}}/users
```
Sample request data:

{
    "name": "John Doe",
    "email": "johndoe.ancheta@gmail.com",
    "password": "testing123",
    "age": 21
}
```

### [POST] Login User
{{url}}/users
```
Sample request data:

{
    "email": "johndoe.ancheta@gmail.com",
    "password": "testing123"
}
```

### [POST] Logout User
{{url}}/users/logout

### [POST] Logout All Users
{{url}}/users/logoutAll

### [POST] Create Task
{{url}}/tasks
```
Sample request data:

{
    "description": "task_one",
    "completed": false
}
```

### [GET] Read Profile
{{url}}/users/me

### [GET] Read User
{{url}}/users/63e724174002fa440481eda7

### [DEL] Delete User
{{url}}/users/me

### [GET] Read Tasks
{{url}}/tasks?sortBy=createdAt:asc

```
Available Queries

// GET /tasks?completed=true
// GET /tasks?limit=10&skip=0
// GET /tasks?sortBy=createdAt:desc
```

### [GET] Read Task
{{url}}/tasks/63e71a0a430a6224bcdf0c7b

### [DEL] Delete Task
{{url}}/tasks/63e7247b95b55b09f8fef2b3

### [PATCH] Update User
{{url}}/users/me
```
{
    "age": 27,
    "name": "Mikey",
    "password": "testing123"
}
```

### [PATCH] Update Task
{{url}}/tasks/63e7247b95b55b09f8fef2b3
```
{
    "completed": false
}
```

### [POST] Upload Avatar
{{url}}/users/me/avatar
KEY: avatar      Value: img.jpg

### [DELETE] Delete Avatar
{{url}}/users/me/avatar

### Access the User Avatar through Browser
http://localhost:3000/users/63e724174002fa440481eda7/avatar
